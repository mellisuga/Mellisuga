
import fs from 'fs';
import path from 'path';
import util from 'util';

export default class {
  static async construct(cms) {
    try {
      let _this = new module.exports(cms);
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cms) {

    var date = new Date();

    let log_dir_path = path.resolve(cms.app_path, 'logs');
    if (!fs.existsSync(log_dir_path)){
      fs.mkdirSync(log_dir_path);
    }

    let log_cms_path = path.resolve(log_dir_path, 'cms');
    if (!fs.existsSync(log_cms_path)){
      fs.mkdirSync(log_cms_path);
    }

    var log_file = fs.createWriteStream(path.resolve(log_cms_path, date+'.log'), {flags : 'w'});
    var log_stdout = process.stdout;

    console.log = function(...args) {
      let output = '';
      for (var a = 0; a < args.length; a++) {
        if (a != 0) output += " ";
        if (args[a] !== null && typeof args[a] === 'object') {
          output += JSON.stringify(args[a], null, 2);
        } else {
          output += args[a];
        }
      }
      log_file.write(util.format(output) + '\n');
      log_stdout.write(util.format(output) + '\n');
    };

    this.log_web_path = path.resolve(log_dir_path, 'web');
    if (!fs.existsSync(this.log_web_path)){
      fs.mkdirSync(this.log_web_path);
    }

    let cur_log_path = path.resolve(this.log_web_path, ""+date);
    if (!fs.existsSync(cur_log_path)) fs.mkdirSync(cur_log_path);

    cms.app.post("/mellisuga-report-error", cms.admin.auth.orize_gen(["content_management"]), function(req, res) {
      let data = JSON.parse(req.body.data).error; 
      let href_log_path = path.resolve(cur_log_path, encodeURIComponent(data.href));
      if (!fs.existsSync(href_log_path)) fs.writeFileSync(href_log_path, data.href+"\n", "utf8");

      if (typeof data.msg === "object" && data.msg !== null) {
        let pmsg = "";
        for (let m in data.msg) {
          if (typeof data.msg[m] === "object" && data.msg[m] !== null) {
            pmsg += JSON.stringify(data.msg[m], null, 2)+"\n";
          } else if (Array.isArray(data.msg[m])) {
            pmsg += JSON.stringify(data.msg[m], null, 2)+"\n";
          } else {
            pmsg += data.msg[m]+" "; 
          }
        }
        data.msg = pmsg;
      }

      if (Array.isArray(data.msg))  {
        let pmsg = "";
        for (let m = 0; m < data.msg.length; m++) {
          if (typeof data.msg[m] === "object" && data.msg[m] !== null) pmsg += JSON.stringify(data.msg[m], null, 2)+"\n";
          if (Array.isArray(data.msg[m])) pmsg += JSON.stringify(data.msg[m], null, 2)+"\n";
        }
        data.msg = pmsg;
      }

      fs.appendFileSync(href_log_path, data.msg+"\n");

      res.send("ok");
    });
  }
}

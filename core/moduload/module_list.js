
import fs from 'fs-extra';
import path from 'path';

import Module from './module.js';

export default class ModuleList {
  constructor(full_path, cms, cfg) {
    this.all_modules = [];
    this.list = [];
    this.full_path = full_path;
    this.cms = cms;
    this.name = this.full_path.substr(this.full_path.lastIndexOf('/') + 1);
    this.cfg = cfg;
  }

  static async init(full_path, cms, cfg) {
    try {
      let this_class = new ModuleList(full_path, cms, cfg);
      let all_modules = this_class.all_modules = this_class.load();
/*
      for (let m = 0; m < all_modules.length; m++) {
        let modl = await Module.init({ ...cfg, ...all_modules[m] }, cms);
        this_class.list.push(modl);
      }
*/
      return this_class;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }

  load() {
    var list = [];
    var this_class = this;

    console.log("LOADING MODULES IN `"+this.full_path+"`");

    fs.readdirSync(this.full_path).forEach(file => {
      var module_path = path.resolve(this_class.full_path, file);
      var module_index_path = path.resolve(module_path, 'index.js');
      var module_config_path = path.resolve(module_path, 'module.json');
      var lstat = fs.lstatSync(module_path);
      if (lstat.isDirectory() || lstat.isSymbolicLink()) {
        if (fs.existsSync(module_index_path)){
          var module = {
            index: module_index_path,
            full_path: module_path,
            name: file,
            parent_list: this_class,
            loaded: false
          };

          if (fs.existsSync(module_config_path)) {
            module.config = JSON.parse(fs.readFileSync(module_config_path, "utf8"));
            module.priority = module.config.priority;

            if (!module.config.after) module.config.after = "Last";
          }

          if (typeof module.priority === "undefined") module.priority = Number.MAX_SAFE_INTEGER;

          list.push(module);
        }
      }
    });

    list.sort(function(a, b) {
      if (!Number.isInteger(a.priority) && Number.isInteger(b.priority)) {
        return -1;
      } else if (!Number.isInteger(b.priority) && Number.isInteger(a.priority)) {
        return 1;
      } else if (!Number.isInteger(a.priority) && !Number.isInteger(b.priority)){
        return 0;
      } else {
        if (a.priority < b.priority) {
          return -1;
        } else if (b.priority < a.priority) {
          return 1;
        } else {
          return 0;
        }
      }
    });
    console.log("QUEUE:");
    for (let i = 0; i < list.length; i++) {
      let item = list[i];
      console.log(">>", item.name, item.priority);
    }

    return list;
  }

  async add(name, template) {
    try {
      const nmodul_path = path.resolve(this.full_path, name);
      if (!fs.existsSync(nmodul_path)) {
        const template_path = path.resolve(new URL('.', import.meta.url).pathname, "default_template")

        fs.copySync(template_path, nmodul_path)
        let nmod = await Module.init({
          ...this.cfg,
          name: name,
          full_path: nmodul_path,
          parent_list: this
        }, this.cms);
        this.list.push(nmod);
        console.log(nmod.data());
        return nmod.data();
      } else {
        return undefined;
      }
    } catch (err) {
      console.error(err)
      return undefined;
    }
  }

  remove(name) {
    for (let l = 0; l < this.list.length; l++) {
      if (this.list[l].name === name) {
        this.list[l].destroy();
        this.list.splice(l, 1);
        return { msg: "success" };
      }
    }
  }

  async reload(name) {
    try {
      const nmodul_path = path.resolve(this.full_path, name);
      for (let l = 0; l < this.list.length; l++) {
        if (this.list[l].name === name) {
          console.log(this.list[l].name);
          console.log(this.list[l].object.destroy);
          if (typeof this.list[l].object.destroy == 'function' ) {
            this.list[l].object.destroy();
          }

          await this.list[l].reload();
          return { msg: "success" };
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  module_exists(mname) {
    for (let m = 0; m < this.all_modules.length; m++) {
      let mod = this.all_modules[m];
      if (mod.name === mname) return true;
    }
    return false;
  }

  module_loaded(mname) {
    for (let m = 0; m < this.list.length; m++) {
      let mod = this.list[m];
      if (mod && mod.name === mname) return true;
    }
    return false;
  }

  get_module(mname) {
    for (let m = 0; m < this.list.length; m++) {
      let mod = this.list[m];
      if (mod.name === mname) return mod.object;
    }
    return false;
  }
}

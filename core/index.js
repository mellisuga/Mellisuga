import fs from 'fs';
import path from 'path';

import Moduload from './moduload/index.js'
import Aura from 'pg-aura';
import Router from './router/index.js';
import PagesIO from 'flypages';

/**
 * Main class for the whole library.
 * @class Mellisuga
 * @hideconstructor
 */
export default class Core {
  /**
   * @static
   * @async
   * @method construct
   * @memberof Mellisuga
   * @arg {Object} cfg Configuration 
   * @arg {String} cfg.host HTTP server adress i.e. `127.0.0.1`
   * @arg {Integer} cfg.port HTTP server port i.e. `8080`
   * @arg {String} cfg.app_path Full path of the main project directory i.e. `new URL('.', import.meta.url).pathname`
   * @arg {Boolean} [cfg.dev_mode] Development mode. Enables dev tools. Default: false
   * @arg {Boolean} [cfg.disable_super] Disables super user functions.
   * @arg {Object} [cfg.extra_context] Extra properties to add to page contexts
   * @arg {Array.<{name: String, cfg: Object}>} modules Configuration objects for WAF modules
   * @returns {Mellisuga}
   */
  static async construct(cfg) {
    try {
      let this_class = undefined;
      let _this = this_class = new Core(cfg);

      /**
        Configuration object loaded from the env.json file. TODO: rename to `env`...
        @name Mellisuga#config
        @type {Object}
      */
      let config = _this.config = await _this.load_config();
      if (config.nopg) _this.nopg = true;

      /**
        Module loader
        @name Mellisuga#modules
        @type {Moduload}
      */
      let modules = _this.modules = await Moduload.construct(_this, cfg);
      await modules.load_after("Start", _this);


      if (!_this.config.db_user || !_this.config.db_user || !_this.config.db_user) {
        console.log("Database not properly configured in `env.json`!");
        console.log("You might want to run `mellisuga env create`.");
//        process.exit();

        _this.config.nopg = true;
        _this.nopg = true;
      }

      /**
        Postgresql database connection object used to create and manipulate tables.
        @name Mellisuga#aura
        @type {Object}
      */
      let aura = undefined
      console.log("AURA CONNECT", !_this.nopg);
      if (!_this.nopg) {
        aura = _this.aura = await Aura.connect({
          host_addr: "127.0.0.1",
          host_port: 5432,
          db_user: _this.config.db_user,
          db_pwd: _this.config.db_pwd,
          db_name: _this.config.db_name
        });
      }
      await modules.load_after("Aura", _this);

      /**
        A high level router that uses exress
        @name Mellisuga#router
        @type {Router}
      */
      let router = _this.router = await Router.init(_this.host, _this.port);
      /**
        Express app
        @name Mellisuga#app
        @type {Object}
      */
      _this.app = router.app;
      _this.app.cms = _this; // TODO: remove this and pass WAF to all modules
      await modules.load_after("Router", _this);

      _this.cfg.aliases = {
        core: path.resolve(_this.app_path, "mellisuga_modules", "web", "core"),
        mellisuga: path.resolve(_this.app_path, "mellisuga_modules", "web", "core"),
      }

      /**
        Web development system focused on pages.
        @name Mellisuga#pages
        @type {Object}
      */
      let pages_io = this_class.pages = await PagesIO.init(router.app, _this.cfg);
      await modules.load_after("Pages", _this);

// TODO: move all globals to adequate WAF modules
  /*    
      pages_io.serve_globals(
        "/mellisuga_globals",
        path.resolve(new URL('.', import.meta.url).pathname, "globals"), {
          context: {
            someval: "aaa"
          },
          app_path: _this.app_path,
          globals_path: _this.globals_path,
          modules_path: path.resolve(_this.app_path, "mellisuga_modules"),
          dev_mode: cfg.dev_mode,
          aliases: _this.cfg.aliases
        }
      );
*/
      await modules.load_after("RouterListen", this_class);

      let dirs_cfg = {
        auth: _this.auth,
        globals_path: this_class.globals_path
      }
      if (cfg.root_page) dirs_cfg.root_page = cfg.root_page;

      router.use(
        '/g',
        Router.static(this_class.globals_path)
      );

      /**
        An object representing a list of pages from your app directory. This object can be used to manipulate those pages.
        @name Mellisuga#page_list
        @type {Object}
      */
      this_class.page_list = pages_io.serve_dirs('/', path.resolve(this_class.app_path, 'pages'), dirs_cfg);
      await modules.load_after("PagesServed", _this);


      if (modules.module_exists("flyadmin")) {
        await router.listen(modules.get_module("flyadmin").auth);
      } else {
        await router.listen();
      }

      await modules.load_after("Last", _this);

      return this_class;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(cfg) {
    this.modules_cfg = cfg.modules || [];


    this.cms_path = cfg.cms_path;
  /**
    Full path of the main project directory as configured.
    @name Mellisuga#app_path
    @type {String}
  */
    this.app_path = cfg.app_path;

    this.config_path = cfg.config_path = path.resolve(cfg.app_path, 'env.json');
    this.pages_path = cfg.pages_path = path.resolve(cfg.app_path, 'pages');
    this.templates_path = cfg.templates_path = path.resolve(cfg.app_path, 'templates');
    this.globals_path = cfg.globals_path = path.resolve(cfg.app_path, 'globals');

    this.lang = cfg.lang;
   
    cfg.admin_path = this.admin_path = "/mellisuga";
    /**
      HTTP server adress
      @name Mellisuga#host
      @type {String}
    */
    this.host = cfg.host;
    /**
      HTTP server port
      @name Mellisuga#port
      @type {Integer}
    */
    this.port = cfg.port;

    this.dev_mode = cfg.dev_mode;
    this.disable_super = cfg.disable_super;

    this.cfg = cfg;

    let _this = this;

    this.server_time = Date.now();
    setInterval(function () {
      _this.server_time = Date.now();
    }, 1000);

  }

  /**
   * @method get_module_cfg 
   * @memberof Mellisuga
   * @arg {String} mname Name of the module
   * @description Get the description of a scpecific module
   * @returns {Object}
   */
  get_module_cfg(mname) {
    for (let c = 0; c < this.modules_cfg.length; c++) {
      let mcfg = this.modules_cfg[c];
      if (mcfg.name === mname) {
        return mcfg.cfg;
      }
    }
  }

  /**
   * @async
   * @method load_config 
   * @memberof Mellisuga
   * @description Load env.json file from the app directory. TODO: rename to `load_env`!
   * @returns {Object}
   */
  async load_config() {
    try {
      let config = undefined;
      if (fs.existsSync(this.config_path)) {
        config = JSON.parse(fs.readFileSync(this.config_path, 'utf8'));
      } else {
        console.log("env.json does not exist");
        //process.exit();
        config = { nopg: true }
      }
      return config;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }


  /**
   * @async
   * @method save_config 
   * @memberof Mellisuga
   * @description Save env.json file to the app directory. TODO: rename to `save_env`!
   */
  async save_config() {
    try {
      fs.writeFileSync(this.config_path, JSON.stringify(this.config, null, 4), 'utf8');
    } catch (e) {
      console.error(e.stack);
    }
  }
}
